package companysearch.component;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import companysearch.model.Company;
import companysearch.service.opendata.OpenDataService;

@ExtendWith(MockitoExtension.class)
class CompanyComponentTest {

	@Mock
	OpenDataService openDataService;
	@Mock
	ElasticsearchRestTemplate elasticsearchRestTemplate;
	@Captor
	ArgumentCaptor<List<Company>> iterableArgumentCaptor;

	@InjectMocks
	CompanyComponent companyComponent;


	@Test
	void shouldInsertAllGivenEntities() {
		//given
		int elementCount = 321123;
		Stream<String> stream = IntStream.range(0, elementCount).mapToObj(String::valueOf);
		when(openDataService.getCompaniesNames()).thenReturn(stream);
		//when
		companyComponent.loadDataFromExternalSource();
		//then
		verify(elasticsearchRestTemplate, atLeastOnce()).save(iterableArgumentCaptor.capture());
		long sum = iterableArgumentCaptor.getAllValues().stream()
				.mapToLong(Collection::size)
				.sum();
		assertThat(sum).isEqualTo(elementCount);
	}

	@Test
	void shouldInsertInBatches() {
		//given
		Stream<String> stream = IntStream.range(0, 321123).mapToObj(String::valueOf);
		when(openDataService.getCompaniesNames()).thenReturn(stream);
		//when
		companyComponent.loadDataFromExternalSource();
		//then
		verify(elasticsearchRestTemplate, atLeastOnce()).save(iterableArgumentCaptor.capture());
		iterableArgumentCaptor.getAllValues().stream()
				.mapToLong(Collection::size)
				.forEach(sum -> assertThat(sum).isLessThanOrEqualTo(10000));
	}

}
