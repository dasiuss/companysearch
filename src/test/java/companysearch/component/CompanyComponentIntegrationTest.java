package companysearch.component;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

import java.util.List;

import companysearch.controller.CompanyController;
import companysearch.model.Company;

@SpringBootTest
class CompanyComponentIntegrationTest {

	private static final String TEST_COUNTRY = "Test Country";
	@Autowired
	ElasticsearchRestTemplate elasticsearchRestTemplate;
	@Autowired
	CompanyController companyController;
	@Autowired
	CompanyComponent companyComponent;

	@Test
	void shouldFindSingleCountry_whenOneInserted() {
		//given
		elasticsearchRestTemplate.save(new Company(TEST_COUNTRY));
		elasticsearchRestTemplate.indexOps(Company.class).refresh();
		//when
		List<Company> foundCompanies = companyController.search(TEST_COUNTRY).getBody();
		//then
		assertThat(foundCompanies)
				.isNotNull()
				.hasSizeGreaterThan(0);
		assertThat(foundCompanies.get(0).getName()).isEqualTo(TEST_COUNTRY);
		//cleanup
		foundCompanies.forEach(c -> elasticsearchRestTemplate.delete(c));
	}

	@ParameterizedTest
	@CsvSource({
			"AARE KABEL SOASEPA-TEISTE TALU,AARE KABEL SOASEPA-TEISTE TALU",
			"AARE KABEL SOASEPA-TEISTE,AARE KABEL SOASEPA-TEISTE TALU",
			"AARE KABEL SOASEPA TEISTE TALU,AARE KABEL SOASEPA-TEISTE ALU",
			"AARE KABEL SOASEPATEISTE TALU,AARE KABEL SOASEPA-TEISTE TALU",
			"Puukko Etnerprises OÜ,Puukko Enterprises OÜ",
			"Etnerprises Puukko OÜ,Puukko Enterprises OÜ"
	})
	void shouldFindByFuzzyNames_givenInExamples(String pattern, String companyName) {
		//given
		elasticsearchRestTemplate.save(new Company(companyName));
		elasticsearchRestTemplate.indexOps(Company.class).refresh();
		//when
		List<Company> foundCompanies = companyController.search(pattern).getBody();
		//then
		assertThat(foundCompanies)
				.isNotNull()
				.hasSizeGreaterThan(0);
		assertThat(foundCompanies.get(0).getName()).isEqualTo(companyName);
		//cleanup
		foundCompanies.forEach(c -> elasticsearchRestTemplate.delete(c));
	}

}
