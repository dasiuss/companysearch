package companysearch.service.opendata;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import feign.FeignException;
import feign.Request;
import feign.Response;
import lombok.SneakyThrows;

@ExtendWith(MockitoExtension.class)
class OpenDataServiceTest {

	static final String FEIGN_ERROR_MESSAGE = "503 SERVICE_UNAVAILABLE \"Unexpected response from open data service, code: 500\"";
	static final String FILE_ERROR_MESSAGE = "503 SERVICE_UNAVAILABLE \"Unexpected response from open data service, empty zip file\"";

	@Mock
	OpenDataClientProvider.OpenDataClient openDataClient;

	@InjectMocks
	OpenDataService openDataService;


	@Test
	void shouldThrow_whenConnectionRefused() {
		//given
		when(openDataClient.getZippedCompaniesData()).thenThrow(new FeignException.InternalServerError("Could not get data", null));
		//when
		Assertions.assertThatThrownBy(() -> openDataService.getCompaniesNames())
				.hasMessage(FEIGN_ERROR_MESSAGE)
				.isOfAnyClassIn(OpenDataServiceException.class);
		//then throws
	}

	@Test
	void shouldThrow_whenFileInvalid() throws IOException {
		//given
		InputStream inputStreamMock = new FileInputStream("mvnw");
		Response responseMock = getResponseMock(inputStreamMock);
		when(openDataClient.getZippedCompaniesData()).thenReturn(responseMock);
		//when
		Assertions.assertThatThrownBy(() -> openDataService.getCompaniesNames())
				.hasMessage(FILE_ERROR_MESSAGE)
				.isOfAnyClassIn(OpenDataServiceException.class);
		//then throws
	}

	private Response getResponseMock(InputStream inputStreamMock) {
		return Response.builder()
				.body(inputStreamMock, 0)
				.request(Request.create(Request.HttpMethod.GET, "", Collections.emptyMap(), new byte[0], Charset.defaultCharset()))
				.build();
	}

	@Test
	void shouldSkipHeader() {
		//given
		String csv = "header;data\nname;value";
		Response responseMock = getResponseMock(new ByteArrayInputStream(getZip(csv)));
		when(openDataClient.getZippedCompaniesData()).thenReturn(responseMock);
		//when
		Stream<String> companiesNames = openDataService.getCompaniesNames();
		//then
		List<String> names = companiesNames.collect(Collectors.toList());
		assertThat(names).hasSize(1);
		assertThat(names.get(0)).doesNotContain("header");
	}

	@SneakyThrows
	byte[] getZip(String s) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (ZipOutputStream zos = new ZipOutputStream(baos)) {
			ZipEntry entry = new ZipEntry("in-memory");

			zos.putNextEntry(entry);
			zos.write(s.getBytes());
			zos.closeEntry();
		}
		return baos.toByteArray();
	}

	@Test
	void shouldOnlyGetFirstColumn() {
		//given
		String csv = "header;data\nname;value";
		Response responseMock = getResponseMock(new ByteArrayInputStream(getZip(csv)));
		when(openDataClient.getZippedCompaniesData()).thenReturn(responseMock);
		//when
		Stream<String> companiesNames = openDataService.getCompaniesNames();
		//then
		List<String> names = companiesNames.collect(Collectors.toList());
		assertThat(names).hasSize(1);
		assertThat(names.get(0)).isEqualTo("name");
	}
}
