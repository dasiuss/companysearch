package companysearch.service;

import org.apache.logging.log4j.Logger;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FeignClientLogger extends feign.Logger {
	private final Logger log;
	private final String serviceName;


	@Override
	protected void log(String configKey, String format, Object... args) {
		log.info(() -> serviceName + " -  " + configKey + " " + String.format(format, args));
	}
}
