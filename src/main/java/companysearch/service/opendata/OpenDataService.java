package companysearch.service.opendata;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Stream;
import java.util.zip.ZipInputStream;

import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@RequiredArgsConstructor
public class OpenDataService {

	private static final String CSV_DELIMITER = ";";
	private static final int HEADER_LINES = 1;

	private final OpenDataClientProvider.OpenDataClient openDataClient;


	public Stream<String> getCompaniesNames() {
		try {

			InputStream companiesDataStream = getCompaniesDataStream();

			return extractCompaniesName(companiesDataStream);

		} catch (IOException e) {
			throw new OpenDataServiceException("Could not read data from open data service");
		} catch (FeignException e) {
			throw new OpenDataServiceException("Unexpected response from open data service, code: " + e.status());
		}
	}

	private InputStream getCompaniesDataStream() throws IOException {
		InputStream response = openDataClient.getZippedCompaniesData().body().asInputStream();
		ZipInputStream zipInputStream = new ZipInputStream(response);
		if (zipInputStream.getNextEntry() == null) {
			throw new OpenDataServiceException("Unexpected response from open data service, empty zip file");
		}
		return zipInputStream;
	}

	private Stream<String> extractCompaniesName(InputStream companiesDataStream) {
		return new BufferedReader(new InputStreamReader(companiesDataStream), 10000)
				.lines()
				.skip(HEADER_LINES)
				.map(line -> line.substring(0, line.indexOf(CSV_DELIMITER)));
	}
}
