package companysearch.service.opendata;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

import companysearch.service.FeignClientLogger;
import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.RequestLine;
import feign.Response;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Configuration
class OpenDataClientProvider {

	private static final Duration TIMEOUT = Duration.ofSeconds(30);

	@Bean
	OpenDataClient getOpenDataClient(@Value("${open-data-service.host}") String url) {
		return Feign.builder()
				.encoder(new JacksonEncoder())
				.decoder(new JacksonDecoder())
				.logLevel(Logger.Level.BASIC)
				.logger(new FeignClientLogger(log, "OpenDataClient"))
				.options(new Request.Options((int) TIMEOUT.toMillis(), (int) TIMEOUT.toMillis()))
				.target(OpenDataClient.class, url);
	}

	interface OpenDataClient {
		@RequestLine("GET /andmed/ARIREGISTER/ariregister_csv.zip")
		Response getZippedCompaniesData();
	}
}
