package companysearch.service.opendata;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class OpenDataServiceException extends ResponseStatusException {
	public OpenDataServiceException(String reason) {
		super(HttpStatus.SERVICE_UNAVAILABLE, reason);
	}
}
