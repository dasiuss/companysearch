package companysearch.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Value;

@Value
@Document(indexName = "company")
public class Company {
	@Id
	String id;
	String name;

	public Company(String name) {
		id = null;
		this.name = name;
	}
}
