package companysearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import companysearch.model.Company;

@Repository
public interface CompanyRepository extends ElasticsearchRepository<Company, String> {

}
