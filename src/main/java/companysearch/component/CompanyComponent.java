package companysearch.component;

import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import companysearch.model.Company;
import companysearch.service.opendata.OpenDataService;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class CompanyComponent {
	private static final String NAME_FIELD = "name";

	private final OpenDataService openDataService;
	private final ElasticsearchRestTemplate elasticsearchRestTemplate;

	public List<Company> search(String pattern) {
		NativeSearchQuery query = new NativeSearchQueryBuilder()
				.withQuery(QueryBuilders.matchQuery(NAME_FIELD, pattern).fuzziness(Fuzziness.AUTO))
				.build();

		return elasticsearchRestTemplate.search(query, Company.class)
				.stream()
				.map(SearchHit::getContent)
				.collect(Collectors.toList());
	}


	public void loadDataFromExternalSource() {
		BufferedCompanySaver bufferedSaver = new BufferedCompanySaver();

		try (Stream<String> companiesNames = openDataService.getCompaniesNames()) {
			companiesNames
					.map(Company::new)
					.forEach(bufferedSaver::save);
		}
		bufferedSaver.flush();
	}

	private class BufferedCompanySaver {
		private static final int SAVE_BUFFER_SIZE = 10000;
		private List<Company> buffer = new ArrayList<>(SAVE_BUFFER_SIZE);

		synchronized void save(Company company) {
			buffer.add(company);
			if (buffer.size() >= SAVE_BUFFER_SIZE) {
				elasticsearchRestTemplate.save(buffer);
				buffer = new ArrayList<>(SAVE_BUFFER_SIZE);
			}
		}

		void flush() {
			elasticsearchRestTemplate.save(buffer);
			buffer = new ArrayList<>(SAVE_BUFFER_SIZE);
		}
	}
}

