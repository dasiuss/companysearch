package companysearch.controller;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import companysearch.component.CompanyComponent;
import companysearch.model.Company;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/company")
public class CompanyController {
	private final CompanyComponent companyComponent;

	@PostMapping("/search")
	public ResponseEntity<List<Company>> search(@RequestBody String pattern) {
		return ok(companyComponent.search(pattern));
	}

	@PostMapping("/load-external-data")
	public ResponseEntity<Object> loadDataFromExternalSource() {
		companyComponent.loadDataFromExternalSource();
		return noContent().build();
	}
}
