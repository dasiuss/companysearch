# Company search

### Setup instruction
To test this application use `docker-compose up -d` to set up elasticsearch
and then run Spring boot application with main method in `CompanySearchApplication.java`.


### How to use
First load data from external source, then you can search for company.

Application provides swagger interface on http://localhost:8080/swagger-ui/ for easier testing.

You can also use curl  
Load external data:   
`curl -X POST "http://localhost:8080/api/v1/company/load-external-data"`  
Search for company name:  
`curl -X POST "http://localhost:8080/api/v1/company/search" -d "Etnerprises Puukko OÜ"`
